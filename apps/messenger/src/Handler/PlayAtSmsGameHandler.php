<?php

declare(strict_types=1);

namespace Apero\Messenger\Handler;

use Apero\Messenger\Message\PlayAtSmsGame;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Class PlayAtSmsGameHandler.
 */
final class PlayAtSmsGameHandler implements MessageHandlerInterface
{
    /** @var LoggerInterface */
    private $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param PlayAtSmsGame $message
     */
    public function __invoke(PlayAtSmsGame $message): void
    {
        $this->logger->debug('Message of «{gamer}» for «{game}» received by PlayAtSmsGameHandler', [
            'gamer' => $message->getGamer(),
            'game' => $message->getGame(),
        ]);

        $delay = rand((int) 0.2*10**6, (int) 1*10**6);
        usleep($delay);

        $this->logger->debug('Message of «{gamer}» for «{game}» handled by PlayAtSmsGameHandler in {delay}', [
            'gamer' => $message->getGamer(),
            'game' => $message->getGame(),
            'delay' => sprintf('%fms', $delay/10**3)
        ]);
    }
}
