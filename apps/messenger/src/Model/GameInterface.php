<?php

declare(strict_types=1);

namespace Apero\Messenger\Model;

use Ramsey\Uuid\UuidInterface;

/**
 * Interface GameInterface.
 */
interface GameInterface
{
    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface;

    /**
     * @return string[]
     */
    public function getAnswers(): array;
}
