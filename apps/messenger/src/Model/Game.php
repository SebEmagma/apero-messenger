<?php

declare(strict_types=1);

namespace Apero\Messenger\Model;

use Ramsey\Uuid\UuidInterface;

/**
 * Class Game.
 */
final class Game implements GameInterface
{
    /** @var UuidInterface */
    protected $id;

    /** @var string[] */
    protected $answers;

    /**
     * @param UuidInterface $id
     * @param array         $answers
     */
    public function __construct(UuidInterface $id, array $answers)
    {
        $this->id = $id;
        $this->answers = $answers;
    }

    /**
     * @return UuidInterface
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string[]
     */
    public function getAnswers(): array
    {
        return $this->answers;
    }
}
