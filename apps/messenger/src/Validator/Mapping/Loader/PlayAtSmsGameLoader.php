<?php

declare(strict_types=1);

namespace Apero\Messenger\Validator\Mapping\Loader;

use Apero\Messenger\Message\PlayAtSmsGame;
use Apero\Messenger\Repository\GameRepositoryInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Mapping\Loader\LoaderInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PlayAtSmsGameLoader.
 */
final class PlayAtSmsGameLoader implements LoaderInterface
{
    /** @var GameRepositoryInterface */
    private $gameRepository;

    /**
     * @param GameRepositoryInterface $gameRepository
     */
    public function __construct(GameRepositoryInterface $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }

    /**
     * @param ClassMetadata $metadata
     */
    public function loadClassMetadata(ClassMetadata $metadata): void
    {
        if ($metadata->getClassName() !== PlayAtSmsGame::class) {
            return;
        }

        $gameRepository = $this->gameRepository;

        $metadata->addConstraint(new Assert\Callback([
            'groups' => ['Default'],
            'callback' => function (PlayAtSmsGame $message, ExecutionContextInterface $context, $payload) use ($gameRepository) {
                $game = $gameRepository->find($message->getGame());

                if (null === $game) {
                    $context->buildViolation('This game does not exists')
                        ->atPath('game')
                        ->addViolation()
                    ;

                    return;
                }

                if (!in_array($message->getAnswer(), $game->getAnswers())) {
                    $context->buildViolation('This answer does not exists')
                        ->atPath('answer')
                        ->addViolation()
                    ;
                }
            },
        ]));
    }
}
