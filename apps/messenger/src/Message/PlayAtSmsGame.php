<?php

declare(strict_types=1);

namespace Apero\Messenger\Message;

/**
 * Class PlayAtSmsGame.
 */
final class PlayAtSmsGame
{
    /** @var string */
    protected $gamer;

    /** @var string */
    protected $game;

    /** @var int */
    protected $answer;

    /**
     * @param string $gamer
     * @param string $game
     * @param int    $answer
     */
    public function __construct(string $gamer, string $game, int $answer)
    {
        $this->gamer = $gamer;
        $this->game = $game;
        $this->answer = $answer;
    }

    /**
     * @return string
     */
    public function getGamer(): string
    {
        return $this->gamer;
    }

    /**
     * @return string
     */
    public function getGame(): string
    {
        return $this->game;
    }

    /**
     * @return int
     */
    public function getAnswer(): int
    {
        return $this->answer;
    }
}
