<?php

declare(strict_types=1);

namespace Apero\Messenger\Repository;

use Apero\Messenger\Model\GameInterface;

/**
 * Interface GameRepositoryInterface.
 */
interface GameRepositoryInterface
{
    /**
     * @return array
     */
    public function findAll(): array;

    /**
     * @param string $id
     *
     * @return null|GameInterface
     */
    public function find(string $id): ?GameInterface;
}
