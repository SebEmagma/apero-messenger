<?php

declare(strict_types=1);

namespace Apero\Messenger\Repository;

use Apero\Messenger\Model\GameInterface;

/**
 * Class GameRepository.
 */
final class GameRepository implements GameRepositoryInterface
{
    /** @var GameInterface[] */
    protected $games;

    /**
     * @param GameInterface[] $games
     */
    public function __construct(array $games)
    {
        foreach ($games as $game) {
            $this->addGame($game);
        }
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->games;
    }

    /**
     * @param string $id
     *
     * @return null|GameInterface
     */
    public function find(string $id): ?GameInterface
    {
        return $this->games[$id];
    }

    /**
     * @param GameInterface $game
     */
    private function addGame(GameInterface $game): void
    {
        $this->games[$game->getId()->toString()] = $game;
    }
}
