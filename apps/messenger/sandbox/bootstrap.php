<?php

require __DIR__.'/../vendor/autoload.php';

$pimple = new \Pimple\Container();
$container = new \Pimple\Psr11\Container($pimple);

// Logger
$pimple['logger'] = function () {
    return new Sandbox\Apero\Messenger\Logger\Logger();
};

// 🚌 Bus
$pimple['bus'] = function ($pimple) use ($container) {
    return new \Symfony\Component\Messenger\MessageBus([
        new \Symfony\Component\Messenger\Middleware\LoggingMiddleware($pimple['logger']),
        new \Symfony\Component\Messenger\Middleware\ValidationMiddleware($pimple['validator']),
        $pimple['messenger.sender_middleware'],
        new \Symfony\Component\Messenger\Middleware\HandleMessageMiddleware(
            new \Symfony\Component\Messenger\Handler\Locator\ContainerHandlerLocator($container)
        ),
    ]);
};

// 🛴 Handler
$pimple['handler.Apero\Messenger\Message\PlayAtSmsGame'] = function ($pimple) {
    return new \Apero\Messenger\Handler\PlayAtSmsGameHandler($pimple['logger']);
};

// Repository
$pimple['repository.game'] = function () {
    $games = array_map(function($game) {
        return new \Apero\Messenger\Model\Game(Ramsey\Uuid\Uuid::fromString($game['id']), $game['answers']);
    }, Symfony\Component\Yaml\Yaml::parse(file_get_contents(__DIR__.'/config/games.yaml')));

    return new \Apero\Messenger\Repository\GameRepository($games);
};

// Validator
$pimple['validator'] = function ($pimple) {
    $metadataLoader = new \Symfony\Component\Validator\Mapping\Loader\LoaderChain([
        new \Apero\Messenger\Validator\Mapping\Loader\PlayAtSmsGameLoader($pimple['repository.game']),
    ]);
    $metadataFactory = new \Symfony\Component\Validator\Mapping\Factory\LazyLoadingMetadataFactory($metadataLoader);

    return \Symfony\Component\Validator\Validation::createValidatorBuilder()
        ->setMetadataFactory($metadataFactory)
        ->getValidator();
};

// 📨 Async
$pimple['messenger.default_transport'] = function () {
    $serializer = new \Symfony\Component\Messenger\Transport\Serialization\Serializer(new \Symfony\Component\Serializer\Serializer(
        [new \Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer()],
        [new \Symfony\Component\Serializer\Encoder\JsonEncoder()]
    ));

    $factory = new \Symfony\Component\Messenger\Transport\AmqpExt\AmqpTransportFactory(
        $serializer,
        $serializer,
        false
    );

    return $factory->createTransport($_ENV['AMQP_DSN'], []);
};

$pimple['messenger.sender_middleware'] = function ($pimple) {
    $locator = new class ($pimple['messenger.default_transport']) implements \Symfony\Component\Messenger\Asynchronous\Routing\SenderLocatorInterface {
        /** @var \Symfony\Component\Messenger\Transport\SenderInterface */
        protected $sender;

        /**
         * @param \Symfony\Component\Messenger\Transport\SenderInterface $sender
         */
        public function __construct(\Symfony\Component\Messenger\Transport\SenderInterface $sender)
        {
            $this->sender = $sender;
        }

        /**
         * @param object $message
         *
         * @return null|\Symfony\Component\Messenger\Transport\SenderInterface
         */
        public function getSenderForMessage($message): ?\Symfony\Component\Messenger\Transport\SenderInterface
        {
            return $this->sender;
        }
    };

    return new \Symfony\Component\Messenger\Asynchronous\Middleware\SendMessageMiddleware($locator);
};
