<?php

declare(strict_types=1);

namespace Sandbox\Apero\Messenger\Command;

use Sandbox\Apero\Messenger\Logger\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\Worker;

/**
 * Class ConsumeMessageCommand.
 */
final class ConsumeMessageCommand extends Command
{
    /** @var Worker */
    protected $worker;

    /** @var Logger */
    protected $logger;

    /**
     * @param Worker $worker
     * @param Logger $logger
     */
    public function __construct(Worker $worker, Logger $logger)
    {
        parent::__construct();

        $this->worker = $worker;
        $this->logger = $logger;
    }

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {
        $this
            ->setName('sandbox:consume-message')
            ->setDescription('Consume messages')
        ;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->logger->setLogger(new ConsoleLogger($output));

        $this->worker->run();
    }
}
