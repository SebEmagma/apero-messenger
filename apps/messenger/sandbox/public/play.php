<?php

require __DIR__.'/../bootstrap.php';

use Apero\Messenger\Message\PlayAtSmsGame;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

// Buffered logger
$bufferedOutput = new BufferedOutput(OutputInterface::VERBOSITY_DEBUG);
$logger = new \Symfony\Component\Console\Logger\ConsoleLogger($bufferedOutput);
$container->get('logger')->setLogger($logger);

$request = Request::createFromGlobals();

$message = new PlayAtSmsGame(
    $request->query->get('gamer'),
    $request->query->get('game'),
    $request->query->getInt('answer')
);

try {
    $container->get('bus')->dispatch($message);
} catch(\Throwable $exception) {
    $logger->error($exception->getMessage());
}

$response = new Response(nl2br($bufferedOutput->fetch()));
$response->send();
