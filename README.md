# Découverte du composant `symfony/messenger`

## Contexte

Nous découvrirons le composant `symfony/messenger` autour d'une application factice.   
Cette application a pour but de prendre en charge des réponses à des jeux «SMS».  
Le but est juste ici de transférer les réponses à un service tiers habibilité à les traîter.  

## Structure

- `apps/messenger/src` sources de l'application
- `apps/messenger/sandbox` application console `sandbox` permettant de jouer avec le code
- `docker` stack Docker

## Étapes

### Step 1: installation

```make install```

- copie le fichier `env` de Docker
- installe les dépendances via Composer
- copie la commande de test de l'application `sandbox`

### Step 2: ajout d'un message & d'un handler

Le but est créer un message qui sera pris en charge par son handler associé.

### Step 3: création du bus

Le but est de créer le message bus qui prendre en charge nos messages.

### Step 4: rajout de l'asynchrone

Le but est de rajouter un traitement asynchrone des messages.

### Step 5: ajout de RabbitMq