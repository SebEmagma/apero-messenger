.DEFAULT_GOAL := help
MY_COMPOSER := composer

install: docker/.env ## Install
install: apps/messenger/composer.lock
install: apps/messenger/sandbox/Command/TryCommand.php

docker/.env: ## Create docker .env file
	@cp docker/.env.dist docker/.env

apps/messenger/composer.lock: ## Install composer dependencies
	$(MY_COMPOSER) install --working-dir=apps/messenger

apps/messenger/sandbox/Command/TryCommand.php: ## Copy sandbox TryCommand
	@cp apps/messenger/sandbox/Command/TryCommand.php.dist apps/messenger/sandbox/Command/TryCommand.php

launch-consumer: ## Launch sandbox consumer
	cd docker && docker-compose exec php sh -c "php /app/sandbox/console sandbox:consume-message -vvv"

help:
	@grep -E '^[a-z\/\.A-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
